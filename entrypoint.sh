#!/bin/bash

fd=0
if ! [[ -t "$fd" || -p /dev/stdin ]]; then
    source ~/.bashrc
fi

exec "$@"
