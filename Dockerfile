FROM node:latest
ENV DEBIAN_FRONTEND noninteractive
SHELL ["/bin/bash", "-c"]
RUN apt-get update && apt-get install -y vim
COPY ./bashrc /root/.bashrc
COPY ./bashrc.d /root/.bashrc.d/
COPY ./entrypoint.sh /entrypoint.sh
COPY ./shell_test.js /shell_test.js
ENTRYPOINT ["/entrypoint.sh"]
CMD ["/bin/bash"]
