if [ -d $HOME/.bashrc.d ]; then
    for i in $HOME/.bashrc.d/*; do
        source "$i"
    done
    unset i
fi

