const { execFile, exec } = require("child_process");

class ShellOutput {
  constructor(stdout, stderr, command, error) {
    this.stdout = stdout;
    this.stderr = stderr;
    this.command = command;
    this.error = error;
  }
}
async function runExec(command, cwd, callback) {
  const options = {
    cwd,
    maxBuffer: 1024 * 1024
  };
  return new Promise((resolve, reject) => {
    const bashCommand = `bash -c '${command}'`;
    console.log(`Running command '${bashCommand}'`);
    const process = exec(bashCommand, options, (error, stdout, stderr) => {
      const result = new ShellOutput(stdout, stderr, bashCommand);
      if (error) {
        result.error = error;
        reject(result);
      } else {
        resolve(result);
      }
    });
  });
}
async function runExecFile(command, args, cwd) {
  const options = {
    cwd,
    maxBuffer: 1024 * 1024
  };
  return new Promise((resolve, reject) => {
    console.log(`Running command '${command} ${args.join(" ")}'`);
    const process = execFile(
      command,
      args,
      options,
      (error, stdout, stderr) => {
        const result = new ShellOutput(stdout, stderr, command);
        if (error) {
          result.error = error;
          reject(result);
        } else {
          resolve(result);
        }
      }
    );
  });
}
async function run() {
  const bashResult = await runExec("ls", "/");
  console.log(`bash result: ${bashResult.stdout}`);
  const execResult = await runExecFile(
    "/bin/ls",
    ["/"]
  );
  console.log(`execFile result: ${execResult.stdout}`);
}
try {
  run();
} catch (error) {
  console.log(error);
}
